language = {
	suggested_font = "NotoSansCJKtc-Regular.otf",

    -- English (key) / Translation
	game_title = "就拿樹枝",
	game_author = "Moosader Games'",
	game_version = "版本 2.0",
	
	menu_play = "Ludi",
	menu_options = "Agordoj",
	menu_classic = "Tradicia",
	menu_enemy = "Fihundo",
	menu_rpg = "Rolludo",
	
	score = "Score",
	
	menu_play = "玩",
	menu_options = "設置",
	menu_classic = "傳統",
	menu_enemy = "壞狗",
	menu_rpg = "角色扮演",
	
	hp = "HP"
}


