#include <SDL.h>
#include "SDL_image.h"

#include "Kuko/base/Application.hpp"
#include "Kuko/base/Sprite.hpp"
#include "Kuko/utilities/Logger.hpp"
#include "Kuko/managers/ImageManager.hpp"
#include "Kuko/managers/StateManager.hpp"
#include "Kuko/managers/InputManager.hpp"
#include "Kuko/managers/FontManager.hpp"
#include "Kuko/managers/LanguageManager.hpp"
#include "Kuko/managers/LuaManager.hpp"
#include "Kuko/managers/SoundManager.hpp"

#include "states/TitleState.hpp"
#include "states/GameClassicState.hpp"
#include "states/GameEnemyState.hpp"
#include "states/GameRpgState.hpp"
#include "states/GameBattleState.hpp"

#include "entities/Player.hpp"

#include <iostream>
#include <string>

#include <stdlib.h>
#include <time.h>

void SetupStaticManagers();
void CleanupStaticManagers();

int main()
{
    srand( time( NULL ) );
    Logger::Setup();
    Logger::Out( "Pickin' Sticks LXXIV" );

    kuko::Application::Start( "Pickin' Sticks LXXIV", 320, 480 );

    SetupStaticManagers();

    kuko::StateManager stateManager;
    stateManager.PushState( "title", new TitleState() );
    stateManager.PushState( "gameClassic", new GameClassicState() );
    stateManager.PushState( "gameEnemy", new GameEnemyState() );
    stateManager.PushState( "gameRpg", new GameRpgState() );
    stateManager.PushState( "gameBattle", new GameBattleState() );
    stateManager.SwitchState( "gameBattle" );

    while ( !stateManager.IsDone() )
    {
        kuko::Application::TimerStart();
        stateManager.UpdateCurrentState();

        kuko::Application::BeginDraw();
        stateManager.DrawCurrentState();

        kuko::Application::EndDraw();
        kuko::Application::TimerUpdate();
    }

    stateManager.Cleanup();
    CleanupStaticManagers();
    Logger::Cleanup();
    kuko::Application::End();

    return 0;
}


void SetupStaticManagers()
{
    kuko::InputManager::Setup();
    kuko::LuaManager::Setup();
//    kuko::LanguageManager::AddLanguage( "mandarin_traditional", "languages/mandarin_traditional.lua" );
//    kuko::LanguageManager::AddLanguage( "mandarin_pinyin", "languages/mandarin_pinyin.lua" );
//    kuko::LanguageManager::AddLanguage( "esperanto", "languages/esperanto.lua" );
    kuko::LanguageManager::AddLanguage( "english", "languages/english.lua" );
    kuko::FontManager::AddFont( "main", "font/" + kuko::LanguageManager::GetSuggestedFont(), 28 );
}

void CleanupStaticManagers()
{
    kuko::LuaManager::Cleanup();
    kuko::FontManager::Cleanup();
    kuko::ImageManager::Cleanup();
    kuko::SoundManager::Cleanup();
    kuko::Application::End();
}
