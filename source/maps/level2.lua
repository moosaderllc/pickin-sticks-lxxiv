level = {
  version = "1.1",
  luaversion = "5.1",
  orientation = "orthogonal",
  width = 8,
  height = 8,
  tilewidth = 40,
  tileheight = 40,
  properties = {},
  tilesets = {
    {
      name = "standard",
      firstgid = 1,
      tilewidth = 40,
      tileheight = 40,
      spacing = 0,
      margin = 0,
      image = "tileset.png",
      imagewidth = 120,
      imageheight = 80,
      properties = {},
      tiles = {
        {
          id = 5,
          properties = {
            ["solid"] = "1"
          }
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Tile Layer 1",
      x = 0,
      y = 0,
      width = 8,
      height = 8,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        6, 6, 3, 2, 2, 2, 2, 2,
        6, 6, 3, 2, 2, 2, 2, 2,
        3, 3, 3, 3, 2, 2, 2, 2,
        1, 1, 3, 3, 3, 2, 2, 2,
        1, 1, 1, 3, 3, 3, 2, 2,
        1, 1, 1, 1, 3, 3, 3, 3,
        1, 1, 1, 1, 1, 3, 6, 6,
        1, 1, 1, 1, 1, 3, 6, 6
      }
    }
  }
}
