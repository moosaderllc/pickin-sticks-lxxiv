#ifndef _GAMECLASSICSTATE
#define _GAMECLASSICSTATE

#include "../Kuko/states/IState.hpp"
#include "../Kuko/base/Sprite.hpp"
#include "../Kuko/entities/Map.hpp"
#include "../Kuko/entities/BaseEntity.hpp"
#include "../Kuko/widgets/UILabel.hpp"
#include "../entities/Player.hpp"
#include "../entities/Item.hpp"

class GameClassicState : public kuko::IState
{
    public:
    //IState() { m_isDone = false; m_isSetup = false; m_gotoState = ""; }
    virtual ~GameClassicState();

    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw();

    //virtual std::string GetNextState() { return m_gotoState; }

    protected:
    //bool m_isDone;
    //bool m_isSetup;
    //std::string m_gotoState;

    Player m_player;
    Item m_stick;
    kuko::Map m_map;
    kuko::UILabel m_score;

    std::vector< kuko::BaseEntity > lstTiles;
};

#endif
