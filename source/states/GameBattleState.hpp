#ifndef _GAMEBATTLESTATE
#define _GAMEBATTLESTATE

#include "../Kuko/states/IState.hpp"
#include "../Kuko/base/Sprite.hpp"
#include "../Kuko/entities/Map.hpp"
#include "../Kuko/entities/BaseEntity.hpp"
#include "../Kuko/widgets/UILabel.hpp"
#include "../Kuko/managers/MenuManager.hpp"

class GameBattleState : public kuko::IState
{
    public:
    //IState() { m_isDone = false; m_isSetup = false; m_gotoState = ""; }
    virtual ~GameBattleState();

    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw();

    //virtual std::string GetNextState() { return m_gotoState; }

    protected:
    //bool m_isDone;
    //bool m_isSetup;
    //std::string m_gotoState;
    kuko::MenuManager menuManager;
    kuko::UILabel m_enemyHP;
    kuko::UILabel m_playerHP;

    kuko::Sprite m_background;
    kuko::Sprite m_player;
    kuko::Sprite m_enemy;
};

#endif

