#ifndef PS_TITLESTATE
#define PS_TITLESTATE

#include <SDL_ttf.h>

#include "../Kuko/states/IState.hpp"
#include "../Kuko/base/Sprite.hpp"
#include "../Kuko/widgets/UIImage.hpp"
#include "../Kuko/widgets/UIButton.hpp"
#include "../Kuko/widgets/UILabel.hpp"

#include "../Kuko/managers/MenuManager.hpp"

class TitleState : public kuko::IState
{
    public:
    //IState() { m_isDone = false; m_isSetup = false; m_gotoState = ""; }
    virtual ~TitleState();

    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw();

    //virtual std::string GetNextState() { return m_gotoState; }

    protected:
    //bool m_isDone;
    //bool m_isSetup;
    //std::string m_gotoState;
    kuko::MenuManager menuManager;

    virtual void SetupFirstMenu();
    virtual void SetupGameSubMenu();
};

#endif
