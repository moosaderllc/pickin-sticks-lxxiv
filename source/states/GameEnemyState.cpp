#include "GameEnemyState.hpp"

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/managers/FontManager.hpp"
#include "../Kuko/managers/InputManager.hpp"
#include "../Kuko/managers/SoundManager.hpp"

GameEnemyState::~GameEnemyState() { ; }

void GameEnemyState::Setup()
{
    Logger::Out( "Setup", "GameEnemyState" );
    kuko::ImageManager::AddTexture( "player_sheet", "gfx/player_animated.png" );
    kuko::ImageManager::AddTexture( "enemy_sheet", "gfx/dog_animated.png" );
    kuko::ImageManager::AddTexture( "stick_sheet", "gfx/stick_filmstrip.png" );
    kuko::ImageManager::AddTexture( "tileset", "gfx/tileset.png" );
    kuko::SoundManager::AddSound( "collect", "audio/blip.wav" );
    kuko::SoundManager::AddSound( "bad", "audio/baddog.wav" );

    m_player.Setup( "player", kuko::ImageManager::GetTexture( "player_sheet" ),
        { 40 * 2, 40 * 2, 40, 40 }, false );

    m_enemy.Setup( "enemy", kuko::ImageManager::GetTexture( "enemy_sheet" ),
        { 40 * 5, 40 * 5, 40, 40 }, false );

    m_stick.Setup( "m_stick", kuko::ImageManager::GetTexture( "stick_sheet" ), { 0, 0, 40, 40 } );

    int w = kuko::Application::GetScreenWidth();
    int h = kuko::Application::GetScreenHeight();
    int mapSize = 0;
    if ( w > h )
    {
        mapSize = h;
    }
    else
    {
        mapSize = w;
    }

    m_map.Setup( "map", "maps/level2.lua", kuko::ImageManager::GetTexture( "tileset" ), mapSize );

    do
    {
        m_stick.PlaceRandomly( 0, 0, m_map.GetWidth(), m_map.GetHeight() );
    } while ( m_map.IsCollision( m_stick.GetPosition() ) );

    m_score.Setup( "score", "Score: 0", { 10, 330, 120, 20 }, false, { 255, 255, 255, 255 }, kuko::FontManager::GetFont( "main" ) );
}

void GameEnemyState::Cleanup()
{
    kuko::ImageManager::ClearTextures();
}

void GameEnemyState::Update()
{
    kuko::InputManager::Update();

    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();

    if ( input[ kuko::TAP ].down )
    {
    }
    else if( input[ kuko::SECONDARY_TAP ].down )
    {
        m_gotoState = "title";
    }

    if ( input[ kuko::MOVE_UP ].down && !m_map.IsCollision( m_player.MoveCheck( UP ) ) )
    {
        m_player.Move( UP );
    }
    else if ( input[ kuko::MOVE_DOWN ].down && !m_map.IsCollision( m_player.MoveCheck( DOWN ) ) )
    {
        m_player.Move( DOWN );
    }
    else if ( input[ kuko::MOVE_LEFT ].down && !m_map.IsCollision( m_player.MoveCheck( LEFT ) ) )
    {
        m_player.Move( LEFT );
    }
    else if ( input[ kuko::MOVE_RIGHT ].down && !m_map.IsCollision( m_player.MoveCheck( RIGHT ) ) )
    {
        m_player.Move( RIGHT );
    }

    Direction enemyChoice = m_enemy.DecideDirection( m_stick );
    if ( !m_map.IsCollision( m_enemy.MoveCheck( enemyChoice ) ) )
    {
        m_enemy.Move( enemyChoice );
    }

    m_player.Update( m_map.GetWidth(), m_map.GetHeight() );
    m_enemy.Update( m_map.GetWidth(), m_map.GetHeight() );
    m_stick.Update();

    // I'm being a lazy coder tonight - clean this up.
    if ( !m_player.IsMoving() && m_stick.IsCollision( m_player ) )
    {
        kuko::SoundManager::PlaySound( "collect" );
        do
        {
            m_stick.PlaceRandomly( 0, 0, m_map.GetWidth(), m_map.GetHeight() );
        } while ( m_map.IsCollision( m_stick.GetPosition() ) );

        m_player.IncrementScore();
        const std::string score = "Score: " + Logger::IntToString( m_player.GetScore() );
        m_score.ChangeText( score );
    }
    if ( !m_enemy.IsMoving() && m_stick.IsCollision( m_enemy ) )
    {
        kuko::SoundManager::PlaySound( "bad" );
        do
        {
            m_stick.PlaceRandomly( 0, 0, m_map.GetWidth(), m_map.GetHeight() );
        } while ( m_map.IsCollision( m_stick.GetPosition() ) );

        m_player.DecrementScore();
        const std::string score = "Score: " + Logger::IntToString( m_player.GetScore() );
        m_score.ChangeText( score );
    }
}

void GameEnemyState::Draw()
{
    m_map.Draw();
    m_stick.Draw();
    m_player.Draw();
    m_enemy.Draw();
    m_score.Draw();
}
