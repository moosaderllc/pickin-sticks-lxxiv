#include "TitleState.hpp"

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/managers/InputManager.hpp"
#include "../Kuko/managers/FontManager.hpp"
#include "../Kuko/managers/LanguageManager.hpp"
#include "../Kuko/managers/SoundManager.hpp"
#include "../Kuko/utilities/Logger.hpp"
#include "../Kuko/base/Application.hpp"

TitleState::~TitleState()
{
}

void TitleState::Setup()
{
    Logger::Out( "Setup", "TitleState" );
    m_gotoState = "";

    // Load Assets
    kuko::ImageManager::AddTexture( "cat", "gfx/battlecat.png" );
    kuko::ImageManager::AddTexture( "titlebg", "gfx/titlescreen.png" );
    kuko::ImageManager::AddTexture( "uibutton", "gfx/button_default.png" );
    kuko::SoundManager::AddSound( "sfx", "audio/blip.wav" );
    kuko::SoundManager::AddMusic( "bgmus", "audio/newbattle.wav" );

    SetupFirstMenu();

    //kuko::SoundManager::PlayMusic( "bgmus", true );
}

void TitleState::Cleanup()
{
    kuko::ImageManager::ClearTextures();
}

void TitleState::Update()
{
    kuko::InputManager::Update();

    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();

    if ( input[ kuko::TAP ].down )
    {
        Logger::Out( "Tap", "TitleState::Update" );

        LOG( "Tap! " + I2S( input[ kuko::TAP ].actionX ) + ", " + I2S( input[ kuko::TAP ].actionY ) );

        if ( menuManager.IsButtonClicked( "btnPlay", input[ kuko::TAP ].actionX, input[ kuko::TAP ].actionY ) )
        {
            SetupGameSubMenu();
        }
        else if ( menuManager.IsButtonClicked( "btnOptions", input[ kuko::TAP ].actionX, input[ kuko::TAP ].actionY ) )
        {
            Logger::Out( "Click button" );
            kuko::SoundManager::PlaySound( "sfx" );
        }
        else if ( menuManager.IsButtonClicked( "btnPlayClassic", input[ kuko::TAP ].actionX, input[ kuko::TAP ].actionY ) )
        {
            m_gotoState = "gameClassic";
        }
        else if ( menuManager.IsButtonClicked( "btnPlayEnemy", input[ kuko::TAP ].actionX, input[ kuko::TAP ].actionY ) )
        {
            m_gotoState = "gameEnemy";
        }
        else if ( menuManager.IsButtonClicked( "btnPlayRpg", input[ kuko::TAP ].actionX, input[ kuko::TAP ].actionY ) )
        {
            m_gotoState = "gameRpg";
        }
    }
    else if ( input[ kuko::SECONDARY_TAP ].down )
    {
        m_gotoState = "quit";
    }
    else
    {
        menuManager.ResetMouse();
    }
}

void TitleState::SetupFirstMenu()
{
    // Setup Menu
    menuManager.SetupMenu( "menus/titlestate.lua" );
}

void TitleState::SetupGameSubMenu()
{
    // Setup Menu
    menuManager.SetupMenu( "menus/titlestate_gameoptions.lua" );
}

void TitleState::Draw()
{
    menuManager.Draw();
}
