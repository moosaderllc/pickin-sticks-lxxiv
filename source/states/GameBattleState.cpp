#include "GameBattleState.hpp"

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/managers/InputManager.hpp"
#include "../Kuko/managers/LanguageManager.hpp"
#include "../Kuko/managers/FontManager.hpp"


GameBattleState::~GameBattleState()
{
}


void GameBattleState::Setup()
{
    Logger::Out( "Setup", "GameBattleState" );

    kuko::ImageManager::AddTexture( "uibutton", "gfx/button_default.png" );
    kuko::ImageManager::AddTexture( "battle_hud", "gfx/battle_hud.png" );
    kuko::ImageManager::AddTexture( "background", "gfx/battle_bg.png" );
    kuko::ImageManager::AddTexture( "enemy", "gfx/battle_dog.png" );
    kuko::ImageManager::AddTexture( "player", "gfx/battle_cat.png" );

    m_background.SetTexture( kuko::ImageManager::GetTexture( "background" ) );

    int w = kuko::Application::GetScreenWidth();
    int h = kuko::Application::GetScreenHeight();

    m_player.SetTexture( kuko::ImageManager::GetTexture( "player" ) );
    m_player.SetPosition( { w - 96, 100, 92, 86 } );

    m_enemy.SetTexture( kuko::ImageManager::GetTexture( "enemy" ) );
    m_enemy.SetPosition( { 0, 100, 92, 86 } );

    menuManager.SetupMenu( "menus/battlestate.lua" );

    // m_score.Setup( "score", "Score: 0", { 10, 330, 120, 20 }, false, { 255, 255, 255, 255 }, kuko::FontManager::GetFont( "main" ) );
    m_enemyHP.Setup( "health", kuko::LanguageManager::Text( "hp" ) + ":100", { 7, 210, 120, 20 }, false, { 175, 194, 255, 255 }, kuko::FontManager::GetFont( "main" ) );
    m_playerHP.Setup( "health", kuko::LanguageManager::Text( "hp" ) + ":100", { w/2+40, 210, 120, 20 }, false, { 255, 175, 255, 255 }, kuko::FontManager::GetFont( "main" ) );
}

void GameBattleState::Cleanup()
{
    kuko::ImageManager::ClearTextures();
}

void GameBattleState::Update()
{
    kuko::InputManager::Update();

    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();

    if ( input[ kuko::TAP ].down )
    {
        if ( menuManager.IsButtonClicked( "btnAttack", input[ kuko::TAP ].actionX, input[ kuko::TAP ].actionY ) )
        {
        }
        else if ( menuManager.IsButtonClicked( "btnDefend", input[ kuko::TAP ].actionX, input[ kuko::TAP ].actionY ) )
        {
        }
        else if ( menuManager.IsButtonClicked( "btnFlee", input[ kuko::TAP ].actionX, input[ kuko::TAP ].actionY ) )
        {
        }
    }
    else if( input[ kuko::SECONDARY_TAP ].down )
    {
        m_gotoState = "quit";
    }
}

void GameBattleState::Draw()
{
    kuko::ImageManager::Draw( m_background );
    kuko::ImageManager::Draw( m_player );
    kuko::ImageManager::Draw( m_enemy );
    menuManager.Draw();
    m_enemyHP.Draw();
    m_playerHP.Draw();
}
