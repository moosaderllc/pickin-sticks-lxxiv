#include "GameRpgState.hpp"

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/managers/FontManager.hpp"
#include "../Kuko/managers/InputManager.hpp"
#include "../Kuko/managers/SoundManager.hpp"

GameRpgState::~GameRpgState() { ; }

void GameRpgState::Setup()
{
    Logger::Out( "Setup", "GameRpgState" );
    kuko::ImageManager::AddTexture( "player_sheet", "gfx/player_animated.png" );
    kuko::ImageManager::AddTexture( "stick_sheet", "gfx/stick_filmstrip.png" );
    kuko::ImageManager::AddTexture( "tileset", "gfx/tileset.png" );

    kuko::SoundManager::AddSound( "commence_battle", "audio/newbattle.wav" );

    m_player.Setup( "player", kuko::ImageManager::GetTexture( "player_sheet" ),
        { 40 * 3, 40 * 3, 40, 40 }, false );

    int w = kuko::Application::GetScreenWidth();
    int h = kuko::Application::GetScreenHeight();
    int mapSize = 0;
    if ( w > h )
    {
        mapSize = h;
    }
    else
    {
        mapSize = w;
    }

    m_map.Setup( "map", "maps/level3.lua", kuko::ImageManager::GetTexture( "tileset" ), mapSize );

    m_timeout = 0;
    m_chanceForBattle = 5;
}

void GameRpgState::Cleanup()
{
    kuko::ImageManager::ClearTextures();
}

void GameRpgState::Update()
{
    if ( m_timeout > 0 )
    {
        m_timeout -= 1;

        if ( m_timeout == 0 )
        {
            // Next state
            m_gotoState = "gameBattle";
        }
    }
    else
    {
        kuko::InputManager::Update();

        std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();

        if ( input[ kuko::TAP ].down )
        {
        }
        else if( input[ kuko::SECONDARY_TAP ].down )
        {
            m_gotoState = "title";
        }

        if ( input[ kuko::MOVE_UP ].down && !m_map.IsCollision( m_player.MoveCheck( UP ) ) )
        {
            m_player.Move( UP );
        }
        else if ( input[ kuko::MOVE_DOWN ].down && !m_map.IsCollision( m_player.MoveCheck( DOWN ) ) )
        {
            m_player.Move( DOWN );
        }
        else if ( input[ kuko::MOVE_LEFT ].down && !m_map.IsCollision( m_player.MoveCheck( LEFT ) ) )
        {
            m_player.Move( LEFT );
        }
        else if ( input[ kuko::MOVE_RIGHT ].down && !m_map.IsCollision( m_player.MoveCheck( RIGHT ) ) )
        {
            m_player.Move( RIGHT );
        }

        m_player.Update( m_map.GetWidth(), m_map.GetHeight() );

        // Random battle?
        if ( m_player.JustFinishedMoving() )
        {
            Logger::Out( "Player just finished moving" );
            int randomChance = rand() % m_chanceForBattle;
            Logger::Out( "Random chance: " + I2S( randomChance ) );
            if ( randomChance == 0 )
            {
                Logger::Out( "BATTLE!" );
                // Begin Random Battle!
                kuko::SoundManager::PlaySound( "commence_battle" );
                m_timeout = 100;
            }
        }
    }
}

void GameRpgState::Draw()
{
    m_map.Draw();
    m_player.Draw();
}
