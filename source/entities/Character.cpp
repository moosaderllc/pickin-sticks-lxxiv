#include "Character.hpp"

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/base/Application.hpp"

Character::Character()
{
    m_speed = 0.25;
    m_frameCounter = 0;
    m_isSolid = true;
    m_lastMoveCounter = m_moveCounter = 0;
}

bool Character::IsMoving()
{
    return ( m_moveCounter <= 0 );
}

bool Character::JustFinishedMoving()
{
    return ( m_moveCounter <= 0 && m_lastMoveCounter > 0 );
}

void Character::Setup( const std::string& name, SDL_Texture* texture, kuko::PositionRect position, bool center /* = false */  )
{
    m_id = name;
    m_sprite.SetTexture( texture );
    m_position = position;
    if ( center )
    {
        m_position.x -= m_position.w / 2;
        m_position.y -= m_position.h / 2;
    }

    m_sprite.frame = { 0, 0, m_position.w, m_position.h };

    UpdateSprite();
}

void Character::Cleanup()
{
}

void Character::Update( int maxX, int maxY )
{
    // Move character
    m_lastMoveCounter = m_moveCounter;
    if ( m_moveCounter > 0 )
    {
        m_moveCounter -= m_speed;

        if ( m_moveDirection == UP )
        {
            m_position.y -= m_speed;
        }
        else if ( m_moveDirection == DOWN )
        {
            m_position.y += m_speed;
        }
        else if ( m_moveDirection == LEFT )
        {
            m_position.x -= m_speed;
        }
        else if ( m_moveDirection == RIGHT )
        {
            m_position.x += m_speed;
        }

        // Animate sprite while moving
        m_frameCounter += 0.01;
        if ( m_frameCounter >= 2 ) { m_frameCounter = 0; }
        m_sprite.frame.x = int( m_frameCounter ) * m_position.w;
    }

    // Don't allow off-screen
    if ( m_position.x < 0 ) { m_position.x = 0; }
    if ( m_position.y < 0 ) { m_position.y = 0; }
    if ( m_position.x + m_position.w > maxX ) { m_position.x = maxX - m_position.w; }
    if ( m_position.y + m_position.h > maxY ) { m_position.y = maxY - m_position.h; }

    if ( m_moveCounter == 0 )
    {
        // Make sure character is still aligned to grid
        m_position.x = ( int(m_position.x) / 40 ) * 40;
        m_position.y = ( int(m_position.y) / 40 ) * 40;
    }

    UpdateSprite();
}

void Character::Draw()
{
    kuko::ImageManager::Draw( m_sprite );
}

void Character::Move( Direction dir )
{
    // Begin movement
    if ( m_moveCounter <= 0 )
    {
        m_moveCounter = 40;
        m_moveDirection = dir;
    }
}

// Return where player would be once they moved this direction
kuko::PositionRect Character::MoveCheck( Direction dir )
{
    kuko::PositionRect tempRect = m_position;
    if ( dir == UP )
    {
        tempRect.y -= 40;
    }
    else if ( dir == DOWN )
    {
        tempRect.y += 40;
    }
    else if ( dir == LEFT )
    {
        tempRect.x -= 40;
    }
    else if ( dir == RIGHT )
    {
        tempRect.x += 40;
    }

    return tempRect;
}
