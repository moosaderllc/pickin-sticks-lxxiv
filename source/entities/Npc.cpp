#include "Npc.hpp"

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/base/Application.hpp"

Npc::Npc()
{
    m_speed = 0.20;
    m_frameCounter = 0;
    m_isSolid = true;
    m_moveCounter = 0;
}

Direction Npc::DecideDirection( const kuko::BaseEntity& target )
{
    // Dog is getting stuck on nothing?
    // Sometimes it gets misaligned from the grid, affecting its decision-making.
    if ( m_moveCounter <= 0 )
    {
        kuko::PositionRect targetPos = target.GetPosition();

        Logger::Out( "Stick - Me" );
        Logger::Out( I2S( targetPos.x ) + " - " + I2S( m_position.x ) );
        Logger::Out( I2S( targetPos.y ) + " - " + I2S( m_position.y ) );
        if ( int(targetPos.x) < int(m_position.x) )
        {
            Logger::Out( "LEFT" );
            return LEFT;
        }
        else if ( int(targetPos.y) > int(m_position.y) )
        {
            Logger::Out( "DOWN" );
            return DOWN;
        }
        else if ( int(targetPos.x) > int(m_position.x) )
        {
            Logger::Out( "RIGHT" );
            return RIGHT;
        }
        else if ( int(targetPos.y) < int(m_position.y) )
        {
            Logger::Out( "UP" );
            return UP;
        }
        Logger::Out( "NONE" );
    }
    return NONE;
}

void Npc::Update( int maxX, int maxY )
{
    // Move character
    if ( m_moveCounter > 0 )
    {
        m_moveCounter -= m_speed;

        if ( m_moveDirection == UP )
        {
            m_position.y -= m_speed;
        }
        else if ( m_moveDirection == DOWN )
        {
            m_position.y += m_speed;
        }
        else if ( m_moveDirection == LEFT )
        {
            m_position.x -= m_speed;
        }
        else if ( m_moveDirection == RIGHT )
        {
            m_position.x += m_speed;
        }

        // Animate sprite while moving
        m_frameCounter += 0.01;
        if ( m_frameCounter >= 2 ) { m_frameCounter = 0; }
        m_sprite.frame.x = int( m_frameCounter ) * m_position.w;
    }

    // Don't allow off-screen
    if ( m_position.x < 0 ) { m_position.x = 0; }
    if ( m_position.y < 0 ) { m_position.y = 0; }
    if ( m_position.x + m_position.w > maxX ) { m_position.x = maxX - m_position.w; }
    if ( m_position.y + m_position.h > maxY ) { m_position.y = maxY - m_position.h; }

    if ( m_moveCounter == 0 )
    {
        // Make sure character is still aligned to grid
        m_position.x = ( int(m_position.x) / 40 ) * 40;
        m_position.y = ( int(m_position.y) / 40 ) * 40;
    }

    UpdateSprite();
}
