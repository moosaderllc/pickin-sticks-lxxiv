#ifndef _CHARACTER
#define _CHARACTER

#include "../Kuko/entities/BaseEntity.hpp"

enum Direction { NONE, UP, DOWN, LEFT, RIGHT };

class Character : public kuko::BaseEntity
{
    public:
    Character();

    virtual void Setup( const std::string& name, SDL_Texture* texture, kuko::PositionRect position, bool center = false  );
    virtual void Cleanup();

    virtual void Update( int maxX, int maxY );
    virtual void Draw();
    bool IsMoving();
    bool JustFinishedMoving();
    void Move( Direction dir );

    kuko::PositionRect MoveCheck( Direction dir );

    protected:
    //std::string m_id;
    //SDL_Rect m_position;
    //Sprite m_sprite;
    //void UpdateSprite();

    float m_speed;
    float m_moveCounter;
    float m_lastMoveCounter;
    Direction m_moveDirection;
    float m_frameCounter;

};

#endif
