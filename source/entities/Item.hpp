#ifndef _ITEM
#define _ITEM

#include <cstdlib>

#include "../Kuko/entities/BaseEntity.hpp"

class Item : public kuko::BaseEntity
{
    public:
    Item();

    void Setup( const std::string& name, SDL_Texture* texture, kuko::PositionRect position, bool center = false );
    void PlaceRandomly( int minX, int minY, int maxX, int maxY );

    protected:
    //std::string m_id;
    //SDL_Rect m_position;
    //Sprite m_sprite;
    //void UpdateSprite();
};

#endif
