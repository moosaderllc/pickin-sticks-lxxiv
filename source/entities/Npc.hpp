#ifndef _NPC
#define _NPC

#include "Character.hpp"

class Npc : public Character
{
    public:
    Npc();

    Direction DecideDirection( const kuko::BaseEntity& target );

    //virtual void Setup( const std::string& name, SDL_Texture* texture, kuko::PositionRect position, bool center = false  );
    //virtual void Cleanup();

    virtual void Update( int maxX, int maxY );
    //virtual void Draw();
    //bool IsMoving();

    //void MoveUp();
    //void MoveDown();
    //void MoveLeft();
    //void MoveRight();

    //kuko::PositionRect MoveCheck( Direction dir );

    protected:
    //std::string m_id;
    //SDL_Rect m_position;
    //Sprite m_sprite;
    //void UpdateSprite();

    //void Move( Direction dir );
    //float m_speed;
    //float m_moveCounter;
    //Direction m_moveDirection;
    //float m_frameCounter;

};

#endif
