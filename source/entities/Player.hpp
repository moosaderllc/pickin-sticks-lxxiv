#ifndef _PLAYER
#define _PLAYER

#include "Character.hpp"

class Player : public Character
{
    public:
    Player();

    //virtual void Setup( const std::string& name, SDL_Texture* texture, kuko::PositionRect position, bool center = false  );
    //virtual void Cleanup();

    //virtual void Update( int maxX, int maxY );
    //virtual void Draw();
    void IncrementScore();
    void DecrementScore();
    int GetScore();
    int GetHP();
    int GetLevel();
    //bool IsMoving();

    //void MoveUp();
    //void MoveDown();
    //void MoveLeft();
    //void MoveRight();

    //kuko::PositionRect MoveCheck( Direction dir );

    protected:
    //std::string m_id;
    //SDL_Rect m_position;
    //Sprite m_sprite;
    //void UpdateSprite();

    //void Move( Direction dir );
    //float m_speed;
    //float m_moveCounter;
    //Direction m_moveDirection;
    //float m_frameCounter;
    int m_score;
    int m_hp;
    int m_level;
};

#endif
