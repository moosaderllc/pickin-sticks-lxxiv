#include "Item.hpp"

#include "../Kuko/base/Application.hpp"

Item::Item()
{
    m_isSolid = true;
}

void Item::Setup( const std::string& name, SDL_Texture* texture, kuko::PositionRect position, bool center /* = false */  )
{
    m_id = name;
    m_sprite.SetTexture( texture );
    m_position = position;
    if ( center )
    {
        m_position.x -= m_position.w / 2;
        m_position.y -= m_position.h / 2;
    }

    m_sprite.frame = { 0, 0, m_position.w, m_position.h };

    UpdateSprite();
}

void Item::PlaceRandomly( int minX, int minY, int maxX, int maxY )
{
    if ( maxX != 0 )
    {
        m_position.x = (rand() % maxX + minX);
    }
    if ( maxY != 0 )
    {
        m_position.y = (rand() % maxY + minY);
    }

    // Align to grid
    m_position.x = ( int(m_position.x) / 40 ) * 40;
    m_position.y = ( int(m_position.y) / 40 ) * 40;

    // Set random appearance

    m_sprite.frame = { (rand() % 2) * m_position.w, 0, m_position.w, m_position.h };
}
