elements = {	
	{ 
		ui_type = "image", 
		id = "statBg", 
		x = 0, y = 200, width = 320, height = 90, 
		texture_id = "battle_hud" 
	},
	
	{ 
		ui_type = "button", 
		id = "btnAttack", text_id = "battle_attack", 
		x = 10, y = 300, width = 300, height = 50, 
		texture_id = "uibutton", 
		font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255,
		pad_x1 = 14, pad_x2 = 100, pad_y1 = 14, pad_y2 = 14
	},
	
	{ 
		ui_type = "button", 
		id = "btnDefend", text_id = "battle_defend", 
		x = 10, y = 360, width = 300, height = 50, 
		texture_id = "uibutton", 
		font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255,
		pad_x1 = 14, pad_x2 = 100, pad_y1 = 14, pad_y2 = 14
	},
	
	{ 
		ui_type = "button", 
		id = "btnFlee", text_id = "battle_flee", 
		x = 10, y = 420, width = 300, height = 50, 
		texture_id = "uibutton", 
		font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255,
		pad_x1 = 14, pad_x2 = 65, pad_y1 = 14, pad_y2 = 14
	},
}
