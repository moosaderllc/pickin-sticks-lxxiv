# Pickin' Sticks LXXIV

## About Pickin' Sticks LXXIV

PS 74 was originally written in Lua with the Gideros framework. That version is still on GitHub, in this repository, under the ["GiderosVersion" branch](https://github.com/Moosader/Pickin-Sticks-LXXIV/tree/GiderosVersion).

The version here on the "master" branch is using C++, SDL2, and Lua. It is built on top of my [Kuko Framework](https://github.com/Moosader/Kuko), created in 2015 for creating Win/Linux/Mac and Android games.

## About Pickin' Sticks

Any time I am working with a new framework, engine, or library, I make a **Pickin' Sticks** game. Pickin' Sticks is my "Hello World" - a basic game that I can make to get better acquainted with my toolset.

The original Pickin' Sticks was written in 2006, as the second C++ game I had ever written.

There are [many versions of Pickin' Sticks, made by many people](https://www.youtube.com/watch?list=PL6BEB6AFC7BD9B300&v=PYrS54WH96Q), based on my [Beginner's Guide to Game Programming](http://www.moosader.com/learn/) tutorial series from 2008.

## About Moosader

[Moosader.com](http://www.moosader.com/) - A one-woman studio in Kansas City, run by Rachel J. Morris

* **Rachel @ Moosader . com**
* [Moosader GitHub](https://www.github.com/moosader)
* [Rachel's personal GitHub](https://www.github.com/rejcx)
* [@Moosader on Twitter](http://www.twitter.com/moosader)
* [@Rejcx on Twitter](http://www.twitter.com/rejcx)
